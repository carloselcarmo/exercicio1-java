package br.com.Itau;

import sun.awt.X11.XSystemTrayPeer;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class IO
{
    public static Map<String, String> solicitarDadosCliente()
    {
        Scanner scanner = new Scanner(System.in);

        System.out.printf("Digite o CPF: ");
        String cpf = scanner.nextLine();

        System.out.printf("Digite o Nome: ");
        String nome = scanner.nextLine();

        System.out.printf("Digite a Senha: ");
        String senha = scanner.nextLine();

        System.out.printf("Digite a Idade: ");
        Integer idade = scanner.nextInt();

        Map<String, String> dados = new HashMap<>();
        dados.put("nome", nome);
        dados.put("cpf", cpf);
        dados.put("idade", idade.toString());
        dados.put("senha", senha);

        return dados;
    }

    public static Map<String, Double> solicitarValorSaque()
    {
        Scanner scanner = new Scanner(System.in);

        System.out.printf("Digite o valor a ser sacado: ");
        Double valor = scanner.nextDouble();

        Map<String, Double> dados = new HashMap<>();
        dados.put("valor", valor);

        return dados;
    }

    public static Map<String, Double> solicitarValorDeposito()
    {
        Scanner scanner = new Scanner(System.in);

        System.out.printf("Digite o valor a ser depositado: ");
        Double valor = scanner.nextDouble();

        Map<String, Double> dados = new HashMap<>();
        dados.put("valor", valor);

        return dados;
    }

    public static Map<String, Integer> solicitarOpcaoMenu()
    {
        Scanner scanner = new Scanner(System.in);

        System.out.printf("(1) Sacar: ");
        System.out.printf("(2) Depositar: ");
        System.out.printf("(3) Alterar Senha: ");
        System.out.printf("(4) Sair: ");
        System.out.printf("Digite a opção a ser executada: ");
        Integer opcao = scanner.nextInt();

        Map<String, Integer> dados = new HashMap<>();
        dados.put("opcao", opcao);

        return dados;
    }

    public static Map<String, String> solicitarAlteracaoSenha()
    {
        Scanner scanner = new Scanner(System.in);

        System.out.printf("Digite o senha: ");
        String senha = scanner.nextLine();

        System.out.printf("Digite o nova senha: ");
        String novasenha = scanner.nextLine();

        System.out.printf("Digite a confirmação da nova senha: ");
        String confirmacaonovasenha = scanner.nextLine();

        Map<String, String> dados = new HashMap<>();
        dados.put("senha", senha);
        dados.put("novasenha", novasenha);
        dados.put("confirmacaosenha", confirmacaonovasenha);

        return dados;
    }
}
