package br.com.Itau;

public class Cliente
{
    private String cpf;
    private String nome;
    private int idade;
    private String senha;
    private TipoCliente tipo;

    public String getCpf() {
        return cpf;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public TipoCliente getTipo() {
        return tipo;
    }

    public void setTipo(TipoCliente tipo) {
        this.tipo = tipo;
    }

    public Cliente (String cpf, String nome, TipoCliente tipo, int idade, String senha)
    {
        this.cpf = cpf;
        this.nome = nome;
        if(idade >= 18)
            this.idade = idade;
        else
            throw new ArithmeticException("O cliente é menor de idade");
        this.tipo = tipo;
        this.senha = senha;
    }

    public boolean alterarSenha (String senha, String novaSenha, String confirmacaoNovaSenha)
    {
        if(this.senha.trim() == senha.trim())
        {
            if(novaSenha == confirmacaoNovaSenha)
            {
                this.senha = novaSenha;
                return true;
            }
            else
            {
                System.out.println("Confirmação da nova senha não confere com a nova senha");
                return false;
            }
        }
        else
        {
            System.out.println("Não foi possível alterar a senha do cliente");
            return false;
        }
    }
}
