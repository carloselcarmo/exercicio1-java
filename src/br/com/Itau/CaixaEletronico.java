package br.com.Itau;

import java.util.Map;

public class CaixaEletronico
{

    public static void exibirMenu(Conta conta)
    {
        System.out.println("Saldo: " + conta.getSaldo());

        Integer opcao = 0;
        do
        {
            Map<String, Integer> dadosMenu = IO.solicitarOpcaoMenu();
            opcao = dadosMenu.get("opcao");

            if(opcao == 1)
                executarSaque(conta);
            else if(opcao == 2)
                executarDeposito(conta);
            else if(opcao == 3)
                executarAlterarSenha(conta.getCliente());
        }
        while (opcao != 4);
    }

    private static void executarDeposito(Conta conta)
    {
        Map<String, Double> dadosDeposito = IO.solicitarValorDeposito();
        if (conta.depositar(dadosDeposito.get("valor")))
            System.out.println("Saldo: " + conta.getSaldo());
        else
            System.out.println("Depósito não realizado");
    }

    private static void executarSaque(Conta conta)
    {
        Map<String, Double> dadosSaque = IO.solicitarValorSaque();
        if (conta.sacar(dadosSaque.get("valor")))
            System.out.println("Saldo: " + conta.getSaldo());
        else
            System.out.println("Saque não realizado");
    }

    private static void executarAlterarSenha(Cliente cliente)
    {
        Map<String, String> dadosAlterarSenha = IO.solicitarAlteracaoSenha();
        if(cliente.alterarSenha(dadosAlterarSenha.get("senha")
                            , dadosAlterarSenha.get("novasenha")
                            , dadosAlterarSenha.get("confirmacaosenha")))
        {
            System.out.println("Senha Alterada com sucesso");
        }
    }
}
