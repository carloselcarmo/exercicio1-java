package br.com.Itau;

public class Conta
{
    private int agencia;
    private int conta;
    private Cliente cliente;
    private Double saldo;

    public int getAgencia()
    {
        return agencia;
    }

    public void setAgencia(int agencia)
    {
        this.agencia = agencia;
    }

    public int getConta()
    {
        return conta;
    }

    public void setConta(int conta)
    {
        this.conta = conta;
    }

    public Cliente getCliente()
    {
        return cliente;
    }

    public void setCliente(Cliente cliente)
    {
        this.cliente = cliente;
    }

    public double getSaldo()
    {
        return saldo;
    }

    public Conta (int agencia, int conta, Cliente cliente)
    {
        this.agencia = agencia;
        this.conta = conta;
        this.cliente = cliente;
        this.saldo = new Double(0);
    }

    public boolean sacar(Double valor)
    {
        if(valor > 0)
        {
            if(valor <= saldo)
            {
                saldo -= valor;
                return true;
            }
            else
            {
                System.out.println("O cliente não possui valor suficiente para sacar");
            }
        }
        else
        {
            System.out.println("O valor informado deve ser maior que 0");
        }

        return false;
    }

    public boolean depositar(Double valor)
    {
        if(valor > 0)
        {
            saldo += valor;
            return true;
        }
        else
        {
            System.out.println("O valor informado deve ser maior que 0");
        }

        return false;
    }
}
