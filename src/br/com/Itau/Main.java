package br.com.Itau;

import sun.rmi.server.MarshalInputStream;

import java.util.Map;

public class Main {

    public static void main(String[] args)
    {
        Map<String, String> dadosCliente = IO.solicitarDadosCliente();

        try
        {
            Cliente cliente = new Cliente(dadosCliente.get("cpf")
                    , dadosCliente.get("nome")
                    , TipoCliente.Varejo
                    , Integer.parseInt(dadosCliente.get("idade"))
                    , dadosCliente.get("senha"));

            Conta conta = new Conta(1500, 12345, cliente);
            conta.setCliente(cliente);

            CaixaEletronico.exibirMenu(conta);
        }
        catch (ArithmeticException ex)
        {
            System.out.printf(ex.getMessage());
        }
    }
}
